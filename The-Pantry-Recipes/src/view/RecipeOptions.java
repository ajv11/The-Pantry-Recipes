//Not being used now, since I found a better way to make the GUI, but there is some good code here
//for future reference
package view;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class RecipeOptions extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel background;
	GridLayout myGrid;
	public RecipeOptions(){
		//Name top bar
		super("Recipe Choices");
		//Split up the screen into a grid
		GridLayout myGrid = new GridLayout(10,1);
		//Make a panel to display
		background = new JPanel(); 
		//Add layout to the panel
		background.setLayout(myGrid);
		//Add buttons
		for(int i=0;i<10;i++){
			background.add(new JButton("Recipe "+i));
		}
		//Set background
		background.setBackground(Color.BLUE);
		//Set the panel into the content pane/screen
		this.setContentPane(background);
	}
}
 